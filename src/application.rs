// SPDX-License-Identifier: GPL-3.0-or-later
use gettextrs::gettext;
use log::{debug, info};

use adw::prelude::*;
use gtk::{gio, glib};

use crate::config::{APP_ID, PKGDATADIR, PROFILE, VERSION};
use crate::window::Window;

mod imp {
    use super::*;

    use adw::subclass::prelude::*;

    #[derive(Debug, Default)]
    pub struct Application {}

    #[glib::object_subclass]
    impl ObjectSubclass for Application {
        const NAME: &'static str = "Application";
        type Type = super::Application;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for Application {}

    impl ApplicationImpl for Application {
        fn activate(&self) {
            let app = self.obj();
            self.parent_activate();
            debug!("Application::activate");

            if let Some(window) = app.active_window() {
                window.present();
                return;
            }

            let window = Window::new(&app);
            window.present();
        }

        fn startup(&self) {
            debug!("Application::startup");
            let app = self.obj();
            self.parent_startup();

            // Set icons for shell
            gtk::Window::set_default_icon_name(APP_ID);

            app.setup_gactions();
            app.setup_accels();

            init_widgets();
        }
    }

    impl GtkApplicationImpl for Application {}
    impl AdwApplicationImpl for Application {}
}

glib::wrapper! {
    pub struct Application(ObjectSubclass<imp::Application>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionMap, gio::ActionGroup;
}

#[allow(clippy::new_without_default)]
impl Application {
    pub fn new() -> Self {
        glib::Object::builder()
            .property("application-id", APP_ID)
            .property("resource-base-path", "/org/gnome/design/Emblem/")
            .build()
    }

    fn setup_gactions(&self) {
        let actions = [
            gio::ActionEntryBuilder::new("quit")
                .activate(|app: &Self, _, _| app.quit())
                .build(),
            gio::ActionEntryBuilder::new("about")
                .activate(|app: &Self, _, _| app.show_about_dialog())
                .build(),
        ];

        self.add_action_entries(actions);
    }

    // Sets up keyboard shortcuts
    fn setup_accels(&self) {
        self.set_accels_for_action("app.quit", &["<Control>q"]);
        self.set_accels_for_action("win.generate-favicons", &["<Control>f"]);
        self.set_accels_for_action("window.close", &["<Control>w"]);
        self.set_accels_for_action("save-png", &["<Control>s"]);
        self.set_accels_for_action("save-svg", &["<Control><Shift>s"]);
    }

    fn show_about_dialog(&self) {
        let dialog = adw::AboutDialog::builder()
            .application_name(gettext("Emblem"))
            .comments(gettext("Generate project avatars"))
            .application_icon(APP_ID)
            .license_type(gtk::License::Gpl30)
            .website("https://gitlab.gnome.org/World/design/emblem/")
            .issue_url("https://gitlab.gnome.org/World/design/emblem/-/issues/new")
            .version(VERSION)
            .translator_credits(gettext("translator-credits"))
            .developers(vec!["Maximiliano Sandoval <msandova@gnome.org>"])
            .developer_name("Maximiliano Sandoval")
            .designers(vec!["Tobias Bernard <tbernard@gnome.org>"])
            .build();

        dialog.present(&self.active_window().unwrap());
    }

    pub fn run(&self) -> glib::ExitCode {
        info!("Emblem ({})", APP_ID);
        info!("Version: {} ({})", VERSION, PROFILE);
        info!("Datadir: {}", PKGDATADIR);

        ApplicationExtManual::run(self)
    }
}

fn init_widgets() {
    crate::ColorButton::static_type();
    crate::ColorWidget::static_type();
    crate::Export::static_type();
    crate::Generate::static_type();
}
