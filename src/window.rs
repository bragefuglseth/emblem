// SPDX-License-Identifier: GPL-3.0-or-later
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gdk, gio, glib};

use crate::application::Application;
use crate::config::PROFILE;
use crate::export::{Shape, SymbolicColor};
use crate::image;

const FAVICON_SIZES: [u32; 4] = [32, 128, 180, 192];

mod imp {
    use super::*;

    use adw::subclass::prelude::AdwApplicationWindowImpl;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/gnome/design/Emblem/ui/window.ui")]
    pub struct Window {
        #[template_child]
        pub generate: TemplateChild<crate::Generate>,
        #[template_child]
        pub export: TemplateChild<crate::Export>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Window {
        const NAME: &'static str = "Window";
        type Type = super::Window;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();

            klass.install_action_async("win.generate-favicons", None, |obj, _, _| async move {
                if let Err(err) = obj.generate_favicons().await {
                    log::error!("Could not generate favicons: {err}");
                }
            });

            klass.install_action_async("save-png", None, |obj, _, _| async move {
                if let Err(err) = obj.imp().export.save_png_action().await {
                    log::error!("Could not save png: {err}");
                }
            });

            klass.install_action_async("save-svg", None, |obj, _, _| async move {
                if let Err(err) = obj.imp().export.save_svg_action().await {
                    log::error!("Could not save svg: {err}");
                }
            });
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Window {
        fn constructed(&self) {
            let obj = self.obj();
            self.parent_constructed();

            // Devel Profile
            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            let target = gtk::DropTarget::new(
                gio::File::static_type(),
                gdk::DragAction::COPY | gdk::DragAction::MOVE,
            );

            target.connect_drop(glib::clone!(
                #[weak]
                obj,
                #[upgrade_or]
                false,
                move |target, value, _, _| {
                    let drop = target.current_drop().unwrap();

                    // We first try to read if we get a serialized svg.
                    let formats = drop.formats();
                    if formats.contain_mime_type("image/svg+xml") {
                        drop.read_async(
                            &["image/svg+xml"],
                            glib::Priority::default(),
                            gio::Cancellable::NONE,
                            glib::clone!(
                                #[weak]
                                obj,
                                move |res| {
                                    if let Ok((stream, _)) = res {
                                        glib::spawn_future_local(glib::clone!(
                                            #[weak]
                                            obj,
                                            async move {
                                                match crate::utils::read_stream(stream).await {
                                                    Ok(bytes) => obj.imp().generate.set_symbolic(
                                                        glib::Bytes::from_owned(bytes),
                                                    ),
                                                    Err(err) => log::debug!(
                                                        "Error while reading stream: {:?}",
                                                        err
                                                    ),
                                                }
                                            }
                                        ));
                                    }
                                }
                            ),
                        );

                        return true;
                    }

                    // Then we try with a file.
                    if let Ok(file) = value.get::<gio::File>() {
                        let generate = obj.imp().generate.get();
                        glib::spawn_future_local(glib::clone!(
                            #[weak]
                            generate,
                            #[strong]
                            file,
                            async move {
                                match file.load_contents_future().await {
                                    Ok((bytes, _)) => {
                                        generate.set_symbolic(&glib::Bytes::from_owned(bytes))
                                    }
                                    Err(err) => log::error!("Could not load dnd file: {err}"),
                                }
                            }
                        ));

                        return true;
                    }
                    false
                }
            ));

            obj.add_controller(target);

            obj.setup_gactions();
        }
    }

    impl WidgetImpl for Window {}
    impl WindowImpl for Window {}
    impl ApplicationWindowImpl for Window {}
    impl AdwApplicationWindowImpl for Window {}
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl Window {
    pub fn new(app: &Application) -> Self {
        glib::Object::builder().property("application", app).build()
    }

    fn setup_gactions(&self) {
        // Change Symbolic Color
        let variant_ty = glib::VariantTy::STRING;
        let symbolic_color_action = gio::SimpleAction::new_stateful(
            "change-symbolic-color",
            Some(variant_ty),
            &"auto".to_variant(),
        );
        symbolic_color_action.connect_state_notify(glib::clone!(
            #[weak(rename_to = obj)]
            self,
            move |action| {
                let state: String = action.state().unwrap().get().unwrap();
                obj.imp()
                    .export
                    .set_symbolic_color(SymbolicColor::from(state.as_str()));
            }
        ));
        self.add_action(&symbolic_color_action);

        // Change Shape
        let variant_ty = glib::VariantTy::STRING;
        let shape_action = gio::SimpleAction::new_stateful(
            "change-shape",
            Some(variant_ty),
            &"square".to_variant(),
        );
        shape_action.connect_state_notify(glib::clone!(
            #[weak(rename_to = obj)]
            self,
            move |action| {
                let state: String = action.state().unwrap().get().unwrap();
                obj.imp().export.set_shape(Shape::from(state.as_str()));
            }
        ));
        self.add_action(&shape_action);
    }

    async fn generate_favicons(&self) -> anyhow::Result<()> {
        let imp = self.imp();

        let dialog = gtk::FileDialog::new();
        let dest_folder = match dialog.select_folder_future(Some(self)).await {
            Err(err) if err.matches(gtk::DialogError::Dismissed) => return Ok(()),
            res => res?,
        };

        for size in FAVICON_SIZES {
            let dest = dest_folder.child(format!("favicon-{size}x{size}.png"));
            let config = image::Config {
                size,
                border_radius: size / 8,
                symbolic: imp.generate.symbolic(),
                // TODO why is it here?
                symbolic_color: imp.export.symbolic_color(),
                gradient_start: imp.generate.color(),
                gradient_end: imp.generate.gradient(),
            };
            image::save_png(&config, &dest).await?;
        }

        {
            const SVG_SIZE: u32 = 256;
            let dest = dest_folder.child("favicon.svg");
            let config = image::Config {
                size: SVG_SIZE,
                border_radius: SVG_SIZE / 8,
                symbolic: imp.generate.symbolic(),
                symbolic_color: imp.export.symbolic_color(),
                gradient_start: imp.generate.color(),
                gradient_end: imp.generate.gradient(),
            };
            image::save_svg(&config, &dest).await?;
        }

        Ok(())
    }
}
