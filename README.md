<a href="https://flathub.org/apps/details/org.gnome.design.Emblem">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

# Emblem

<img src="https://gitlab.gnome.org/World/design/emblem/raw/master/data/icons/org.gnome.design.Emblem.svg" width="128" height="128" />
<p>Generate project avatars.</p>

## How to set a GitLab Project Avatar

To set a GitLab project avatar one can put a logo.png file at the root of the project, and if there is no manually set project avatar it will be picked automatically.

## Screenshots

<div align="center">
![screenshot](https://gitlab.gnome.org/World/design/emblem/raw/master/screenshots/main_window.png)
</div>

## Hack on Emblem

To build the development version of Emblem and hack on the code see the [general
guide](https://welcome.gnome.org/en/app/Emblem/#getting-the-app-to-build) for
building GNOME apps with Flatpak and GNOME Builder.

## Translations

Helping to translate Emblem or add support to a new language is very welcome.
You can find everything you need at: [l10n.gnome.org/module/emblem/](https://l10n.gnome.org/module/emblem/)

## Code Of Conduct

This project follows the [GNOME Code of Conduct](https://conduct.gnome.org/).
